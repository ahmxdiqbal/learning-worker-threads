import { parentPort, workerData } from 'worker_threads';
import { WorkerObj, Node } from './classes.js';

const obj = new WorkerObj()
obj.nodeArr.push(new Node(1, 1))
obj.nodeArr.push(new Node(2, 1))

function send(to, msg) {
  let inCurrent = false

  for (let i = 0; i < obj.nodeArr.length; i++){
    if(to === obj.nodeArr[i].nodeID){
      console.log("Node " + obj.nodeArr[i].nodeID + " in worker thread " + obj.nodeArr[i].worker_thread + " displays " + msg)
      inCurrent = true
      parentPort.postMessage("exit")
      break
    }
  }
  if(inCurrent == false){
    const type = "to2"
    parentPort.postMessage(
      [
        type,
        to,
        msg
      ]
    )
  }
}

send(3, "Hello Ahmed!")