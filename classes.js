export class WorkerObj {
    nodeArr = []
}

export class Node {
    nodeID
    worker_thread
    constructor(number, worker_thread){
        this.nodeID = number
        this.worker_thread = worker_thread
    }

    get nodeID() {
        return this.nodeID
    }

    get worker_thread(){
        return this.worker_thread
    }
}