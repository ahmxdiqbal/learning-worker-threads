# Steps to use

1. clone this repo.

2. in the last line of the `worker1.js` file, there is a call of the function `send`.

3. you can change the 1st parameter to choose which node will recieve the message, and the 2nd to choose what the message is.

4. run with `node index.js`
