import { exit } from 'process';
import { parentPort, workerData } from 'worker_threads';
import { WorkerObj, Node } from './classes.js';

const obj = new WorkerObj()
obj.nodeArr.push(new Node(3, 2))
obj.nodeArr.push(new Node(4, 2))


parentPort.on("message", (response) => {
  const [ type, to, msg ] = response

  if(type === "to2"){    
    for (let i = 0; i < obj.nodeArr.length; i++){
      if(to === obj.nodeArr[i].nodeID){
        console.log("Node " + obj.nodeArr[i].nodeID + " in worker thread " + obj.nodeArr[i].worker_thread + " displays " + msg)
      }
    }
  }
  parentPort.postMessage("exit")
})
