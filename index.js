import { Worker, workerData } from 'worker_threads'

const worker1 = new Worker('./worker1.js', {
    workerData: 15
});

const worker2 = new Worker('./worker2.js', {
    workerData: 17
});

worker1.on('message', (response) => {
    const [ type, to, msg ] = response
    if(type === "to2"){
        worker2.postMessage(
            [
                type,
                to,
                msg
            ]
        )
        
    }
    else if(response == "exit"){
        process.exit(0)
    }
    
});



worker2.on('message', (response) => {
    if(response == "exit"){
        process.exit(0)
    }
})

